# Leica Camera for Mi 9T/Redmi K20 (davinci) AOSP 13

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/ItzDFPlayer/vendor_davinci-miuicamera -b leica vendor/xiaomi/miuicamera
```

Make these changes in **sm6150-common**

**sm6150.mk**
```
# MiuiCamera
$(call inherit-product, vendor/xiaomi/miuicamera/config.mk)
```

## Support

### https://t.me/itzdfplayer_stash <br>

